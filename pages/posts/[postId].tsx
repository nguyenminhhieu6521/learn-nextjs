import { AdminLayout } from 'components/layout';
import { GetStaticPaths, GetStaticProps, GetStaticPropsContext } from 'next';

export interface PostDetailPageProps {
	post: any;
}

export default function PostDetailPage({ post }: PostDetailPageProps) {
	if (!post) return null;

	return (
		<>
			<h1>Post Detail Page</h1>

			<p>{post.title}</p>
			<p>{post.author}</p>
			<p>{post.description}</p>
		</>
	);
}

PostDetailPage.Layout = AdminLayout;

export const getStaticPaths: GetStaticPaths = async () => {
	const response = await fetch('https://js-post-api.herokuapp.com/api/posts?_page=1');
	const data = await response.json();

	return {
		paths: data.data.map((post: any) => ({ params: { postId: post.id } })),
		fallback: false,
		// false return 404 page when not match routes list
		// blocking run getStaticProps function to fetch data
		// true same blocking but added fallback in router for client show loading
	};
};

export const getStaticProps: GetStaticProps = async (context: GetStaticPropsContext) => {
	const postId = context.params?.postId;
	if (!postId) return { notFound: true };

	const response = await fetch(`https://js-post-api.herokuapp.com/api/posts/${postId}`);
	const data = await response.json();

	return {
		props: {
			post: data,
		},
		revalidate: 60, // if over 60s return old data then update data and saved in catch
	};
};
