import { MainLayout } from 'components/layout';
import { GetStaticProps, GetStaticPropsContext } from 'next';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import useSWR from 'swr';
import Typography from '@mui/material/Typography';

// Dynamic import with ssr=false only use with client side
// const Header = dynamic(import('../../components/common/header'), { ssr: false });
export interface PostListPageProps {
	posts: any[];
}

export default function PostListPage({ posts }: PostListPageProps) {
	const router = useRouter();
	const page = router.query.page;

	// use SWR get api saved server state
	const { data, error, mutate, isValidating } = useSWR(`/students`, {
		revalidateOnFocus: false, // change tab page no fetch data
		// revalidateOnFocus: false, // no fetch data when start page
		// dedupingInterval: 2000, // repeat fetch data is over 2000ms
	});
	// mutate(data, true) if true when mutate will be fetch data

	useEffect(() => {
		if (!page) return;
		// useEffect only run on client side
		// Use SSG + useEffect => CSR (Pagination)
		(async () => {
			const response = await fetch(`https://js-post-api.herokuapp.com/api/posts?_page=${page}`);
			const data = await response.json();
			console.log(data);
		})();
	}, [page]);

	const handleNextClick = () => {
		router.push(
			{
				pathname: '/posts',
				query: {
					page: (Number(router.query.page) || 1) + 1,
				},
			},
			undefined,
			{ shallow: true }
		); // Use shadow true no run getStaticProps
	};

	return (
		<>
			<Typography component="h1" variant="h3" color="primary.main">
				Post List Page
			</Typography>
			<ul>
				{posts.map((post: any) => (
					<li key={post.id}>
						<Link href={`/posts/${post.id}`}>{post.title}</Link>
					</li>
				))}
			</ul>
			<button onClick={handleNextClick}>Next</button>
		</>
	);
}

// Layout
PostListPage.Layout = MainLayout;

export const getStaticProps: GetStaticProps = async (context: GetStaticPropsContext) => {
	const response = await fetch('https://js-post-api.herokuapp.com/api/posts?_page=1');
	const data = await response.json();

	return {
		props: {
			posts: data.data.map((post: any) => ({ id: post.id, title: post.title })),
		},
	};
};

// // Demo SSR
// export const getServerSideProps: GetServerSideProps = async (context: GetServerSidePropsContext) => {
// 	context.res.setHeader('Catch-Control', 's-maxage=5'); // catch in 5s then repeat fetch
// 	context.res.setHeader('Catch-Control', 's-maxage=5 stale-white-revalidate');
// 	// catch in 5s then over 5s return old data then repeat fetch
// 	context.res.setHeader('Catch-Control', 's-maxage=5 stale-white-revalidate=5');
// 	// catch in 5s then in 5s return old data then repeat fetch. If over 10s repeat fetch no return old data
// 	return {
// 		props: {}
// 	}
// };
