import { Box, Container } from '@mui/system';
import { MainLayout } from 'components/layout';
import { Post } from 'model';
import { GetStaticPaths, GetStaticProps, GetStaticPropsContext } from 'next';
import { getPostList } from 'utils';
import { unified } from 'unified';
import remarkParse from 'remark-parse';
import remarkRehype from 'remark-rehype';
import rehypeDocument from 'rehype-document';
import rehypeFormat from 'rehype-format';
import rehypeStringify from 'rehype-stringify';
import remarkToc from 'remark-toc';
import rehypeSlug from 'rehype-slug';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import remarkPrism from 'remark-prism';
import Script from 'next/script';
import { Seo } from 'components/common';

export interface BlogDetailPageProps {
	post: Post;
}

export default function BlogDetailPage({ post }: BlogDetailPageProps) {
	if (!post) return null;

	return (
		<Box>
			<Seo
				data={{
					title: post.title,
					description: post.description,
					url: `${process.env.HOST_URL}/blog/${post.slug}`,
					thumbnailUrl:
						post.image ||
						'https://cdn.getshifter.co/caa65008efb706a8bfc6f7e4045d6a018420c3df/uploads/2020/11/nextjs-1200x733.png',
				}}
			/>

			<Container>
				<h1>Blog Detail Page</h1>

				<p>{post.title}</p>
				<p>{post.author?.name}</p>
				<p>{post.description}</p>
				<div dangerouslySetInnerHTML={{ __html: post.htmlContent || '' }} />
			</Container>

			<Script src="/prism.js" strategy="afterInteractive" />
			{/* afterInteractive load after something important */}
		</Box>
	);
}

BlogDetailPage.Layout = MainLayout;

export const getStaticPaths: GetStaticPaths = async () => {
	const data = await getPostList();

	return {
		paths: data.map((post: Post) => ({ params: { slug: post.slug } })),
		fallback: false,
	};
};

export const getStaticProps: GetStaticProps = async (context: GetStaticPropsContext) => {
	const slug = context.params?.slug;
	if (!slug) return { notFound: true };

	const postList = await getPostList();
	const post = postList.find((post) => post.slug === slug);

	if (!post) return { notFound: true };

	const file = await unified()
		.use(remarkParse)
		.use(remarkToc, { heading: 'agenda.*' })
		.use(remarkPrism)
		.use(remarkRehype)
		.use(rehypeSlug)
		.use(rehypeAutolinkHeadings, { behavior: 'wrap' })
		.use(rehypeDocument, { title: 'Blog Detail Page 👋🌍' })
		.use(rehypeFormat)
		.use(rehypeStringify)
		.process(post.mdContent || '');

	post.htmlContent = file.toString();

	return {
		props: {
			post,
		},
	};
};
