import { Box, Divider } from '@mui/material';
import Typography from '@mui/material/Typography';
import { Container } from '@mui/system';
import { PostItem } from 'components/blog';
import { MainLayout } from 'components/layout';
import { Post } from 'model';
import { GetStaticProps, GetStaticPropsContext } from 'next';
import Link from 'next/link';
import { getPostList } from 'utils';

export interface BlogListPageProps {
	posts: Post[];
}

export default function BlogListPage({ posts }: BlogListPageProps) {
	if (posts.length === 0) return null;
	return (
		<Box>
			<Container>
				<Typography component="h1" variant="h3" color="primary.main">
					Blog
				</Typography>
				<Box component="ul" sx={{ listStyleType: 'none', p: 0 }}>
					{posts.map((post: Post) => (
						<li key={post.id}>
							<Link href={`/blog/${post.slug}`}>
								<PostItem post={post} />
							</Link>

							<Divider sx={{ my: 3 }} />
						</li>
					))}
				</Box>
			</Container>
		</Box>
	);
}

// Layout
BlogListPage.Layout = MainLayout;

export const getStaticProps: GetStaticProps<BlogListPageProps> = async (
	context: GetStaticPropsContext
) => {
	const posts = await getPostList();

	return {
		props: {
			posts,
		},
	};
};
