import { Paper, Typography } from '@mui/material';
import { LoginForm } from 'components/auth';
import { useAuth } from 'hooks';
import { LoginPayload } from 'model';
import { useRouter } from 'next/router';

export default function LoginPage() {
	const router = useRouter();
	const { profile, login } = useAuth({ revalidateOnFocus: false });
	const handleLoginSubmit = async (payload: LoginPayload) => {
		try {
			await login(payload);
			router.push('/');
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<Paper elevation={4} sx={{ maxWidth: '480px', m: 'auto', mt: 8, p: 2 }}>
			<Typography component="h1" variant="h5">
				Login Page
			</Typography>
			<LoginForm onSubmit={handleLoginSubmit} data={{ username: '', password: '' }} />
		</Paper>
	);
}
