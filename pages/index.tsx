import { Box } from '@mui/material';
import { Seo } from 'components/common';
import { Hero, RecentPost } from 'components/home';
import { FeaturedWorks } from 'components/home/featured-works';
import { MainLayout } from 'components/layout';

export default function Home() {
	return (
		<Box>
			<Seo
				data={{
					title: 'Next JS Tutorial | Easy Frontend',
					description:
						'Step by step tutorials to build a full CRUD website using NextJS for beginners',
					url: 'https://learn-nextjs-one-phi.vercel.app/',
					thumbnailUrl: 'https://learn-nextjs-one-phi.vercel.app/',
				}}
			/>
			<Hero />
			<RecentPost />
			<FeaturedWorks />
		</Box>
	);
}

Home.Layout = MainLayout;
