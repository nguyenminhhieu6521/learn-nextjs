import { CacheProvider } from '@emotion/react';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import axiosClient from 'api-client/axios-client';
import { EmptyLayout } from 'components/layout';
import { AppPropsWithLayout } from 'model';
import { SWRConfig } from 'swr';
import { createEmotionCache, theme } from 'utils';
import '../styles/globals.css';
import '../styles/prism.css';

const clientSideEmotionCache = createEmotionCache();

export default function App({
	Component,
	pageProps,
	emotionCache = clientSideEmotionCache,
}: AppPropsWithLayout) {
	const Layout = Component?.Layout ?? EmptyLayout;
	return (
		<CacheProvider value={emotionCache}>
			<ThemeProvider theme={theme}>
				<CssBaseline />

				<SWRConfig value={{ fetcher: (url) => axiosClient.get(url), shouldRetryOnError: false }}>
					<Layout>
						<Component {...pageProps} />
					</Layout>
				</SWRConfig>
			</ThemeProvider>
		</CacheProvider>
	);
}
