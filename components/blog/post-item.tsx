import { Box, Divider, Stack, Typography } from '@mui/material';
import { format } from 'date-fns';
import { Post } from 'model';
import * as React from 'react';

export interface PostItemProps {
	post: Post;
}

export function PostItem({ post }: PostItemProps) {
	return (
		<Box>
			<Typography variant="h5" fontWeight="bold">
				{post.title}
			</Typography>

			<Stack direction="row" my={2}>
				<Typography>{format(new Date(post.publishedDate), 'dd MMM yyyy')}</Typography>
				<Divider orientation="vertical" flexItem sx={{ mx: 3 }} />
				<Typography>{post.tags.join(', ')}</Typography>
			</Stack>

			<Typography variant="body2">{post.description}</Typography>
		</Box>
	);
}
