import { TextField, TextFieldProps } from '@mui/material';
import * as React from 'react';
import { Control } from 'react-hook-form';
import { useController } from 'react-hook-form';

export type InputFieldProps = TextFieldProps & {
	name: string;
	control: Control<any>;
};

export function InputField({
	name,
	control,
	value: externalValue,
	onChange: externalOnchange,
	onBlur: externalOnBlur,
	ref: externalRef,
	...rest
}: InputFieldProps) {
	const {
		field: { value, onChange, onBlur, ref },
		fieldState: { error },
	} = useController({ name, control });
	return (
		<TextField
			label
			fullWidth
			margin="normal"
			size="small"
			value={value}
			onChange={onChange}
			onBlur={onBlur}
			inputRef={ref}
			{...rest}
			error={!!error}
			helperText={error?.message}
		/>
	);
}
