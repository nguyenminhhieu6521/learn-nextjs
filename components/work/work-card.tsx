import { Chip, Typography } from '@mui/material';
import { Box, Stack } from '@mui/system';
import { Work } from 'model';
import Image from 'next/image';
import * as React from 'react';

export interface WorkCardProps {
	work: Work;
}

export function WorkCard({ work }: WorkCardProps) {
	return (
		<Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
			<Box width={{ xs: '100%', sm: '246px' }} flexShrink={0}>
				<Image
					src={work.thumbnailUrl}
					alt={work.thumbnailUrl}
					layout="responsive"
					width={246}
					height={180}
				/>
			</Box>
			<Box>
				<Typography variant="h5" fontWeight="bold">
					{work.title}
				</Typography>
				<Stack direction="row" my={2}>
					<Chip
						label={new Date(Number(work.createdAt)).getFullYear()}
						size="small"
						color="secondary"
					/>
					<Typography sx={{ ml: 2 }}>{work.tabs.join(', ')}</Typography>
				</Stack>
				<Typography>{work.description}</Typography>
			</Box>
		</Stack>
	);
}
