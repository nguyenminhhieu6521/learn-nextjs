import { Box, Stack, Container } from '@mui/material';
import { Footer } from 'components/common';
import Header from 'components/common/header';
import { LayoutProps } from 'model';
import { useEffect } from 'react';

export function MainLayout({ children }: LayoutProps) {
	useEffect(() => {
		console.log('main layout mounting');
		return () => console.log('main layout unmounting');
	}, []);
	return (
		<Stack minHeight="100vh">
			<Header />
			<Box component="main" flexGrow={1}>
				{children}
			</Box>
			<Footer />
		</Stack>
	);
}
