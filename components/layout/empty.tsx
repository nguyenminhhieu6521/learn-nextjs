import { LayoutProps } from 'model';

export function EmptyLayout({ children }: LayoutProps) {
	return (
		<>
			<div>{children}</div>
		</>
	);
}
