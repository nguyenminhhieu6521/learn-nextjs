import { Auth } from 'components/common';
import { LayoutProps } from 'model';

export function AdminLayout({ children }: LayoutProps) {
	return (
		<Auth>
			<div>Header</div>
			<div>Sidebar</div>
			<div>{children}</div>
			<div>Footer</div>
		</Auth>
	);
}
