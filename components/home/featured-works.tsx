import { Box, Typography } from '@mui/material';
import { Container } from '@mui/system';
import { WorkList } from 'components/work';
import { Work } from 'model';

export interface FeaturedWorksProps {}

export function FeaturedWorks(props: FeaturedWorksProps) {
	const workList: Work[] = [
		{
			id: '1',
			title: 'Designing Dashboards',
			tabs: ['Dashboard'],
			shortDescription: '',
			description:
				'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
			createdAt: '1669955852894',
			updatedAt: '1669955852894',
			thumbnailUrl:
				'https://res.cloudinary.com/kimwy/image/upload/v1648712410/learn-nextjs/item1_cbidwn.jpg',
		},
		{
			id: '2',
			title: 'Vibrant Portraits of 2020',
			tabs: ['Illustration'],
			createdAt: '1669955852894',
			updatedAt: '1669955852894',
			shortDescription: '',
			description:
				'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
			thumbnailUrl:
				'https://res.cloudinary.com/kimwy/image/upload/v1648712410/learn-nextjs/item2_usidpx.jpg',
		},
		{
			id: '3',
			title: '36 Days of Malayalam type',
			tabs: ['Typography'],
			createdAt: '1669955852894',
			updatedAt: '1669955852894',
			shortDescription: '',
			description:
				'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
			thumbnailUrl:
				'https://res.cloudinary.com/kimwy/image/upload/v1648712410/learn-nextjs/item3_jlfuun.jpg',
		},
	];
	return (
		<Box component="section" pt={2} pb={4}>
			<Container>
				<Typography textAlign={{ xs: 'center', md: 'left' }} sx={{ mb: 2 }} variant="h5">
					Featured works
				</Typography>
				<WorkList workList={workList} />
			</Container>
		</Box>
	);
}
