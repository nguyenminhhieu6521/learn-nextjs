import * as React from 'react';
import { Box, Container, Stack, Typography } from '@mui/material';
import Image from 'next/image';
import avatar from '../../images/avatar.png';
import Button from '@mui/material/Button';

export function Hero() {
	return (
		<Box component="section" pt={{ xs: 5, md: 18 }} pb={9}>
			<Container>
				<Stack
					direction={{ xs: 'column-reverse', md: 'row' }}
					alignItems={{ xs: 'center', md: 'flex-start' }}
					textAlign={{ xs: 'center', md: 'left' }}
					spacing={5}
				>
					<Box>
						<Typography component="h1" variant="h3" fontWeight="bold" mb={{ xs: 2.5, md: 5 }}>
							Hi, I am John, <br />
							Creative Technologist
						</Typography>
						<Typography mb={{ xs: 2.5, md: 5 }}>
							Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit
							officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud
							amet.
						</Typography>

						<Button variant="contained" size="large">
							Download Resume
						</Button>
					</Box>
					<Box
						sx={{
							minWidth: '240px',
							boxShadow: '-5px 13px',
							color: 'secondary.light',
							borderRadius: '50%',
						}}
					>
						<Image src={avatar} layout="responsive" alt="avatar" />
					</Box>
				</Stack>
			</Container>
		</Box>
	);
}
