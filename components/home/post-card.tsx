import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { Post } from 'model/post';
import { Typography, Divider } from '@mui/material';
import { Stack } from '@mui/system';
import { format } from 'date-fns';
import { PostItem } from 'components/blog/post-item';

export interface PostCartProps {
	post: Post;
}

export function PostCart({ post }: PostCartProps) {
	return (
		<Card>
			<CardContent>
				<PostItem post={post} />
			</CardContent>
		</Card>
	);
}
