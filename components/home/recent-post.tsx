import { Box, Typography } from '@mui/material';
import { Container, Stack } from '@mui/system';
import { Post } from 'model/post';
import Link from 'next/link';
import * as React from 'react';
import { PostCart } from './post-card';

export interface RecentPostProps {}

export function RecentPost(props: RecentPostProps) {
	const posts: Post[] = [
		{
			id: '1',
			title: 'Making a design system from scratch',
			tags: ['Design', 'Pattern'],
			publishedDate: '2022-12-03T10:00:00Z',
			description:
				'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
		},
		{
			id: '2',
			title: 'Creating pixel perfect icons in Figma',
			tags: ['Figma', 'Icon Design'],
			publishedDate: '2022-12-03T10:00:00Z',
			description:
				'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
		},
	];
	return (
		<Box component="section" bgcolor="secondary.light" pt={2} pb={4}>
			<Container>
				<Stack
					direction="row"
					alignItems="center"
					justifyContent={{ xs: 'center', md: 'space-between' }}
					mb={2}
				>
					<Typography variant="h5">Recents post</Typography>
					<Box
						color="secondary.main"
						display={{ xs: 'none', md: 'inline-block' }}
						sx={{ '&:hover': { color: '#FF6464' } }}
					>
						<Link href="/posts">View all</Link>
					</Box>
				</Stack>

				<Stack
					direction={{ xs: 'column', md: 'row' }}
					spacing={3}
					sx={{ '& > div': { width: { sx: '100%', md: '50%' } } }}
				>
					{posts.map((post) => (
						<Box key={post.id}>
							<PostCart post={post} />
						</Box>
					))}
				</Stack>
			</Container>
		</Box>
	);
}
