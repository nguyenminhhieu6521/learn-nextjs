import { Visibility, VisibilityOff } from '@mui/icons-material';
import { Box, Button, IconButton, InputAdornment } from '@mui/material';
import { InputField } from 'components/form';
import { LoginPayload } from 'model';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

export interface LoginFormProps {
	data: LoginPayload;
	onSubmit: (values: LoginPayload) => void;
}

export function LoginForm({ data, onSubmit }: LoginFormProps) {
	const [showPassword, setShowPassword] = useState(false);

	const schema = yup.object().shape({
		username: yup
			.string()
			.required('Please enter username')
			.min(6, 'Username is required to have at least 4 characters'),
		password: yup
			.string()
			.required('Please enter password')
			.min(4, 'Username is required to have at least 6 characters'),
	});

	const { control, handleSubmit } = useForm<LoginPayload>({
		defaultValues: data,
		resolver: yupResolver(schema),
	});

	const handleClickShowPassword = () => {
		setShowPassword(!showPassword);
	};

	const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
		event.preventDefault();
	};

	const handleLoginSubmit = (values: LoginPayload) => {
		console.log(values);

		onSubmit(values);
	};

	return (
		<Box component="form" onSubmit={handleSubmit(handleLoginSubmit)}>
			<InputField name="username" label="username" control={control} />
			<InputField
				name="password"
				label="Password"
				control={control}
				type={!showPassword ? 'password' : 'text'}
				InputProps={{
					endAdornment: (
						<InputAdornment position="end">
							<IconButton
								aria-label="toggle password visibility"
								onClick={handleClickShowPassword}
								onMouseDown={handleMouseDownPassword}
								edge="end"
							>
								{!showPassword ? <VisibilityOff /> : <Visibility />}
							</IconButton>
						</InputAdornment>
					),
				}}
			/>
			<Button fullWidth sx={{ my: 2 }} type="submit" variant="contained">
				Login
			</Button>
		</Box>
	);
}
