import { Facebook, Instagram, Twitter, LinkedIn } from '@mui/icons-material';
import { Box, Stack, Icon } from '@mui/material';
import * as React from 'react';

export function Footer() {
	const socialLinks = [
		{ icon: Facebook, url: 'https://facebook.com' },
		{ icon: Instagram, url: 'https://instagram.com' },
		{ icon: Twitter, url: 'https://twitter.com' },
		{ icon: LinkedIn, url: 'https://linkedIn.com' },
	];
	return (
		<Box component="footer" py={4} textAlign="center">
			<Stack direction="row" justifyContent="center">
				{socialLinks.map((item, idx) => (
					<Box
						key={idx}
						component="a"
						href={item.url}
						rel="noopener noreferer"
						p={2}
						target="_blank"
					>
						<Icon component={item.icon} sx={{ fontSize: '48px' }} />
					</Box>
				))}
			</Stack>
			Copyright ©{new Date().getFullYear()} All rights reserved
		</Box>
	);
}
