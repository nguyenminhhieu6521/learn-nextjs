import { Box } from '@mui/system';

export default function HeaderMobile() {
	return <Box display={{ xs: 'block', md: 'none' }}>Header mobile</Box>;
}
