const ROUTES = [
	{ label: 'Home', path: '/' },
	{ label: 'Blog', path: '/blog' },
	{ label: 'Work', path: '/work' },
];

export default ROUTES;
