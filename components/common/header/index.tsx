import HeaderDesktop from './desktop';
import HeaderMobile from './mobile';

export default function Header() {
	return (
		<>
			<HeaderDesktop />
			<HeaderMobile />
		</>
	);
}
