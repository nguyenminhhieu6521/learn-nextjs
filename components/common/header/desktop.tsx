import { Box, Container, Stack } from '@mui/system';
import Link from 'next/link';
import { useRouter } from 'next/router';
import ROUTES from './routes';

export default function HeaderDesktop() {
	const router = useRouter();
	return (
		<Box display={{ xs: 'none', md: 'block' }} py={2}>
			<Container>
				<Stack flexDirection="row" justifyContent="flex-end" alignItems="center">
					{ROUTES.map((route) => (
						<Box
							key={route.path}
							ml={2}
							fontWeight="medium"
							color={router.pathname === route.path ? 'primary.main' : 'black'}
							sx={{ '&:hover': { color: '#FF6464' } }}
						>
							<Link href={route.path}>{route.label}</Link>
						</Box>
					))}
				</Stack>
			</Container>
		</Box>
	);
}
