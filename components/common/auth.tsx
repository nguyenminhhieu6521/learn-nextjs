import { useAuth } from 'hooks';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

export interface AuthProps {
	children: any;
}

export function Auth({ children }: AuthProps) {
	const { profile, firstLoading } = useAuth();
	const router = useRouter();

	useEffect(() => {
		if (!profile && !firstLoading) router.push('/login');
	}, [profile, router, firstLoading]);

	if (!profile) return <>Loading...</>;

	return <div>{children}</div>;
}
