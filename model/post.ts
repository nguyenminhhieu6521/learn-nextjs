export interface Author {
	name: string;
	title: string;
	profileUrl: string;
	avatarUrl: string;
}
export interface Post {
	id: string | number;
	title: string;
	tags: string[];
	publishedDate: string;
	description: string;
	slug?: string;
	author?: Author;
	mdContent?: string;
	htmlContent?: string;
	thumbnaillUrl?: string;
	image?: string;
}
