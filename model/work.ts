export interface Work {
	id: string | number;
	title: string;
	tabs: string[];
	createdAt: string;
	updatedAt: string;
	shortDescription: string;
	description: string;
	thumbnailUrl: string;
}
