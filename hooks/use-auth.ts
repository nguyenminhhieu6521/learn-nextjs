import { authApi } from 'api-client';
import { LoginPayload } from 'model';
import useSWR from 'swr';
import { PublicConfiguration } from 'swr/dist/types';

const MS_PER_HOURS = 60 * 60 * 1000;

export function useAuth(options?: Partial<PublicConfiguration>) {
	const {
		data: profile,
		error,
		mutate,
	} = useSWR('/profile', {
		dedupingInterval: MS_PER_HOURS,
		revalidateOnFocus: false,
		...options,
	});

	const firstLoading = profile === undefined && error === undefined;

	async function login(payload: LoginPayload) {
		await authApi.login(payload);

		await mutate();
	}

	async function logout() {
		await authApi.logout();
		await mutate(null, false);
	}

	return { profile, error, login, logout, firstLoading };
}
