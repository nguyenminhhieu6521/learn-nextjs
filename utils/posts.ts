import path from 'path';
import fs from 'fs';
import matter from 'gray-matter';
import { Post } from 'model';

const BLOG_FOLDER = path.join(process.cwd(), 'blog');

export async function getPostList(): Promise<Post[]> {
	const postList: Post[] = [];
	const fileNameList = fs.readdirSync(BLOG_FOLDER);
	fileNameList.forEach((filePath) => {
		const { content, excerpt, data } = matter(
			fs.readFileSync(path.join(BLOG_FOLDER, filePath), 'utf-8'),
			{
				excerpt_separator: '<!-- truncate-->',
			}
		);

		postList.push({
			id: filePath,
			slug: data.slug,
			title: data.title,
			author: {
				name: data.author,
				title: data.author_title,
				profileUrl: data.author_url,
				avatarUrl: data.author_image_url,
			},
			tags: data.tags,
			publishedDate: data.date,
			description: excerpt ?? '',
			mdContent: content,
			image: data.image || null,
		});
	});

	return postList;
}
